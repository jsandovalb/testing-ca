package net.touchsf.testingca.data.repository;

import net.touchsf.testingca.data.datasource.CountryDataStoreFactory;
import net.touchsf.testingca.data.mapper.CountryDataMapper;
import net.touchsf.testingca.data.model.CountryEntity;
import net.touchsf.testingca.domain.interactor.CountryListCallback;
import net.touchsf.testingca.domain.repository.CountryRepository;
import net.touchsf.testingca.domain.repository.RepositoryCallback;

import java.util.List;

public class CountryDataRepository implements CountryRepository {

    private CountryDataStoreFactory countryDataStoreFactory;
    private CountryDataMapper countryDataMapper;

    public CountryDataRepository(CountryDataStoreFactory countryDataStoreFactory, CountryDataMapper countryDataMapper) {
        this.countryDataStoreFactory = countryDataStoreFactory;
        this.countryDataMapper = countryDataMapper;
    }

    @Override
    public void loadCountries(final CountryListCallback countryListCallback) {
        countryDataStoreFactory.create().loadCountries(new RepositoryCallback() {
            @Override
            public void onCountryListSuccess(List<CountryEntity> countries) {
                countryListCallback.onCountryListSuccess(countryDataMapper.transformFromList(countries));
            }

            @Override
            public void onCountryListError(String error) {
                countryListCallback.onCountryListError(error);
            }

            @Override
            public void onComplete() {
                countryListCallback.onComplete();
            }
        });
    }

}
