package net.touchsf.testingca.data.datasource;

import net.touchsf.testingca.domain.repository.RepositoryCallback;

public interface CountryDataStore {

    void loadCountries(RepositoryCallback repositoryCallback);

}
