package net.touchsf.testingca.data.mapper;

import net.touchsf.testingca.data.model.CountryEntity;
import net.touchsf.testingca.domain.model.Country;

import java.util.ArrayList;
import java.util.List;

public class CountryDataMapper {

    public Country transform(CountryEntity entity) {
        Country country = new Country();
        if (entity != null) {
            country.setName(entity.getName());
            country.setAlpha2Code(entity.getAlpha2Code());
            country.setAlpha3Code(entity.getAlpha3Code());
            country.setCapital(entity.getCapital());
            country.setRegion(entity.getRegion());
            country.setFlag(entity.getFlag());
        }
        return country;
    }

    public List<Country> transformFromList(List<CountryEntity> entities) {
        List<Country> countries = new ArrayList<>();
        if (entities != null) {
            for (CountryEntity item : entities) {
                countries.add(transform(item));
            }
        }
        return countries;
    }

}
