package net.touchsf.testingca.data.datasource.cloud;

import net.touchsf.testingca.app.AppController;
import net.touchsf.testingca.app.di.component.DaggerActivityComponent;
import net.touchsf.testingca.data.datasource.CountryDataStore;
import net.touchsf.testingca.data.model.CountryEntity;
import net.touchsf.testingca.domain.model.Country;
import net.touchsf.testingca.domain.repository.RepositoryCallback;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CountryCloudDataStore implements CountryDataStore {

    @Inject ApiService apiService;

    public CountryCloudDataStore() {
        DaggerActivityComponent.builder()
                .appComponent(AppController.getAppComponent())
                .build().inject(this);
    }

    @Override
    public void loadCountries(final RepositoryCallback repositoryCallback) {
        apiService.requestCountries()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CountryEntity>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<CountryEntity> countries) {
                        repositoryCallback.onCountryListSuccess(countries);
                    }

                    @Override
                    public void onError(Throwable e) {
                        repositoryCallback.onCountryListError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        repositoryCallback.onComplete();
                    }
                });
    }

}
