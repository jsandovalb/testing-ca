package net.touchsf.testingca.data.datasource.cloud;

import net.touchsf.testingca.data.model.CountryEntity;
import net.touchsf.testingca.domain.model.Country;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ApiService {

    @GET("rest/v2/region/Americas")
    Observable<List<CountryEntity>> requestCountries();

}
