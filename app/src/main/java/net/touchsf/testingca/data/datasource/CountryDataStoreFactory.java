package net.touchsf.testingca.data.datasource;

import net.touchsf.testingca.data.datasource.cloud.CountryCloudDataStore;

public class CountryDataStoreFactory {

    public CountryDataStore create() {
        return new CountryCloudDataStore();
    }

}
