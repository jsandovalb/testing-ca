package net.touchsf.testingca.presentation.presenter;

import net.touchsf.testingca.data.datasource.CountryDataStoreFactory;
import net.touchsf.testingca.data.mapper.CountryDataMapper;
import net.touchsf.testingca.data.repository.CountryDataRepository;
import net.touchsf.testingca.domain.interactor.CountryListCallback;
import net.touchsf.testingca.domain.interactor.CountryListInteractor;
import net.touchsf.testingca.domain.model.Country;
import net.touchsf.testingca.domain.repository.CountryRepository;
import net.touchsf.testingca.presentation.base.BasePresenter;
import net.touchsf.testingca.presentation.view.CountryListView;

import java.util.List;

import javax.inject.Inject;

public class CountryListPresenter extends BasePresenter<CountryListView> implements CountryListCallback {

    private CountryDataStoreFactory countryDataStoreFactory;
    private CountryDataMapper countryDataMapper;
    private CountryListInteractor countryListInteractor;

    @Inject
    public CountryListPresenter(CountryDataStoreFactory factory, CountryDataMapper mapper, CountryListInteractor interactor) {
        this.countryDataStoreFactory = factory;
        this.countryDataMapper = mapper;
        this.countryListInteractor = interactor;
    }

    public void loadCountries() {
        getMvpView().showLoading();

        CountryRepository countryRepository = new CountryDataRepository(countryDataStoreFactory, countryDataMapper);
        countryListInteractor.setCountryRepository(countryRepository);
        countryListInteractor.loadCountries(this);
    }

    @Override
    public void onCountryListSuccess(List<Country> countries) {
        getMvpView().onSuccess(countries);
    }

    @Override
    public void onCountryListError(String error) {
        getMvpView().onError(error);
    }

    @Override
    public void onComplete() {
        getMvpView().hideLoading();
    }
}
