package net.touchsf.testingca.presentation.ui.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import net.touchsf.testingca.R;
import net.touchsf.testingca.domain.model.Country;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryItemHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvCountryName) TextView tvCountryName;
    @BindView(R.id.tvCountryCode) TextView tvCountryCode;

    public CountryItemHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Country item) {
        tvCountryName.setText(item.getName());
        tvCountryCode.setText(item.getAlpha2Code());
    }

}
