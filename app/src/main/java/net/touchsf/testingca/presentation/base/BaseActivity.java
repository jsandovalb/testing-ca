package net.touchsf.testingca.presentation.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import net.touchsf.testingca.app.AppController;
import net.touchsf.testingca.app.di.component.ActivityComponent;
import net.touchsf.testingca.app.di.component.DaggerActivityComponent;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);
        initDagger();
        init();
    }

    private void initDagger() {
        activityComponent = DaggerActivityComponent.builder()
                .appComponent(((AppController) getApplication()).getAppComponent())
                .build();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    public ActivityComponent getComponent() {
        return activityComponent;
    }

    protected abstract void init();

    protected abstract int getLayout();

}
