package net.touchsf.testingca.presentation.base;

import android.app.Activity;
import android.content.Context;

public interface BaseView {

    Context getContext();

    Activity getActivity();

}
