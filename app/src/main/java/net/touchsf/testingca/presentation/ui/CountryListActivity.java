package net.touchsf.testingca.presentation.ui;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import net.touchsf.testingca.R;
import net.touchsf.testingca.domain.model.Country;
import net.touchsf.testingca.presentation.base.BaseActivity;
import net.touchsf.testingca.presentation.presenter.CountryListPresenter;
import net.touchsf.testingca.presentation.ui.adapter.CountryListAdapter;
import net.touchsf.testingca.presentation.view.CountryListView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class CountryListActivity extends BaseActivity implements CountryListView {

    private static final String TAG = CountryListActivity.class.getSimpleName();

    @Inject CountryListPresenter presenter;
    @Inject CountryListAdapter adapter;
    @BindView(R.id.rvCountries) RecyclerView rvCountries;

    @Override
    protected void init() {
        getComponent().inject(this);
        presenter.attachView(this);

        initViews();
        requestCountries();
    }

    private void initViews() {
        rvCountries.setLayoutManager(new LinearLayoutManager(this));
        rvCountries.setHasFixedSize(true);
        rvCountries.setAdapter(adapter);
    }

    private void requestCountries() {
        presenter.loadCountries();
    }

    @Override
    public void showLoading() {
        Log.i(TAG, "showing loader");
    }

    @Override
    public void hideLoading() {
        Log.i(TAG, "hiding loader");
    }

    @Override
    public void onSuccess(List<Country> countries) {
        Log.i(TAG, "countries:" + countries.toString());
        adapter.setCountryList(countries);
    }

    @Override
    public void onError(String error) {
        Log.e(TAG, "error: " + error);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_country_list;
    }
}
