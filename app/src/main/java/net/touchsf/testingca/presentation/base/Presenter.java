package net.touchsf.testingca.presentation.base;

public interface Presenter<V extends BaseView> {

    void attachView(V view);

    void detachView();

}
