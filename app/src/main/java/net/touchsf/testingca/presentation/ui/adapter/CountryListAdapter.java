package net.touchsf.testingca.presentation.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import net.touchsf.testingca.R;
import net.touchsf.testingca.domain.model.Country;
import net.touchsf.testingca.presentation.ui.holder.CountryItemHolder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class CountryListAdapter extends RecyclerView.Adapter<CountryItemHolder> {

    private List<Country> countryList = new ArrayList<>();

    @Inject
    public CountryListAdapter() {
    }

    @Override
    public CountryItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CountryItemHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_country, parent, false));
    }

    @Override
    public void onBindViewHolder(CountryItemHolder holder, int position) {
        holder.bind(countryList.get(position));
    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
