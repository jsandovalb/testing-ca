package net.touchsf.testingca.presentation.view;

import net.touchsf.testingca.domain.model.Country;
import net.touchsf.testingca.presentation.base.BaseView;

import java.util.List;

public interface CountryListView extends BaseView {

    void showLoading();

    void hideLoading();

    void onSuccess(List<Country> countries);

    void onError(String error);

}
