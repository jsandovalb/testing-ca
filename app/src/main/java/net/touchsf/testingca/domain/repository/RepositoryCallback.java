package net.touchsf.testingca.domain.repository;

import net.touchsf.testingca.data.model.CountryEntity;
import net.touchsf.testingca.domain.model.Country;

import java.util.List;

public interface RepositoryCallback {

    void onCountryListSuccess(List<CountryEntity> countries);

    void onCountryListError(String error);

    void onComplete();

}
