package net.touchsf.testingca.domain.interactor;

import net.touchsf.testingca.domain.repository.CountryRepository;

import javax.inject.Inject;

public class CountryListInteractor {

    private CountryRepository countryRepository;

    @Inject
    public CountryListInteractor() {
    }

    public void setCountryRepository(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public void loadCountries(CountryListCallback countryListCallback) {
        countryRepository.loadCountries(countryListCallback);
    }

}
