package net.touchsf.testingca.domain.interactor;

import net.touchsf.testingca.domain.model.Country;

import java.util.List;

public interface CountryListCallback {

    void onCountryListSuccess(List<Country> countries);

    void onCountryListError(String error);

    void onComplete();

}
