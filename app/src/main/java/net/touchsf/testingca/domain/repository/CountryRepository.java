package net.touchsf.testingca.domain.repository;

import net.touchsf.testingca.domain.interactor.CountryListCallback;

public interface CountryRepository {

    void loadCountries(CountryListCallback countryListCallback);

}
