package net.touchsf.testingca.app.di.module;

import android.app.Application;
import android.content.Context;

import net.touchsf.testingca.data.datasource.cloud.ApiService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    private Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application getApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Context getContext() {
        return mApplication;
    }
}
