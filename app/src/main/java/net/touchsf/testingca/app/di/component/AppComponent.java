package net.touchsf.testingca.app.di.component;

import android.app.Application;
import android.content.Context;

import net.touchsf.testingca.app.di.module.AppModule;
import net.touchsf.testingca.app.di.module.MapperModule;
import net.touchsf.testingca.app.di.module.NetModule;
import net.touchsf.testingca.data.datasource.CountryDataStoreFactory;
import net.touchsf.testingca.data.datasource.cloud.ApiService;
import net.touchsf.testingca.data.datasource.cloud.CountryCloudDataStore;
import net.touchsf.testingca.data.mapper.CountryDataMapper;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class, MapperModule.class})
public interface AppComponent {

    void inject(Application application);

    Application getApplication();

    Context getContext();



}
