package net.touchsf.testingca.app;

import android.app.Application;

import net.touchsf.testingca.app.di.component.AppComponent;
import net.touchsf.testingca.app.di.component.DaggerAppComponent;
import net.touchsf.testingca.app.di.module.AppModule;

public class AppController extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initDagger();

    }

    private void initDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        appComponent.inject(this);
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
