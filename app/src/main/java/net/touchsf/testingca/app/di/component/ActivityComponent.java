package net.touchsf.testingca.app.di.component;

import net.touchsf.testingca.app.di.PerActivityScope;
import net.touchsf.testingca.app.di.module.MapperModule;
import net.touchsf.testingca.app.di.module.NetModule;
import net.touchsf.testingca.data.datasource.CountryDataStoreFactory;
import net.touchsf.testingca.data.datasource.cloud.ApiService;
import net.touchsf.testingca.data.datasource.cloud.CountryCloudDataStore;
import net.touchsf.testingca.data.mapper.CountryDataMapper;
import net.touchsf.testingca.presentation.ui.CountryListActivity;

import dagger.Component;

@PerActivityScope
@Component(dependencies = AppComponent.class, modules = {MapperModule.class, NetModule.class})
public interface ActivityComponent {

    void inject(CountryListActivity activity);

    void inject(CountryCloudDataStore cloudDataStore);

    ApiService getApiService();

    CountryDataStoreFactory getCountryDataStoreFactory();

    CountryDataMapper getCountryDataMapper();

}
