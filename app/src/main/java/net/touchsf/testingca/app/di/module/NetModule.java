package net.touchsf.testingca.app.di.module;

import net.touchsf.testingca.app.di.PerActivityScope;
import net.touchsf.testingca.data.datasource.CountryDataStoreFactory;
import net.touchsf.testingca.data.datasource.cloud.ApiService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetModule {

    @PerActivityScope
    @Provides
    CountryDataStoreFactory provideCountryDataStoreFactory() {
        return new CountryDataStoreFactory();
    }

    @PerActivityScope
    @Provides
    ApiService provideApiService() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("http://restcountries.eu/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ApiService.class);
    }

}