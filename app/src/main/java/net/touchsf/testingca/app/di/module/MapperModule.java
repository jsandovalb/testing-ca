package net.touchsf.testingca.app.di.module;

import net.touchsf.testingca.app.di.PerActivityScope;
import net.touchsf.testingca.data.mapper.CountryDataMapper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MapperModule {

    @PerActivityScope
    @Provides
    CountryDataMapper provideCountryDataMapper() {
        return new CountryDataMapper();
    }

}
